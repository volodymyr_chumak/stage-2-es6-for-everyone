import { controls } from '../../constants/controls';

class FighterDetails {
  #_maxHealth;
  #side;
  #_id;
  blockIsActive;
  lastCritTime;

  constructor(fighter, side) {
    this.#_maxHealth = fighter.health;
    this.#side = side;
    this.#_id = fighter._id;
    this.blockIsActive = false;
  }

  get maxHealth() {
    return this.#_maxHealth;
  }

  get side() {
    return this.#side;
  }

  get _id() {
    return this.#_id;
  }
}

export async function fight(firstFighter, secondFighter) {
  return new Promise((resolve) => {
    const firstFighterDetails = new FighterDetails(firstFighter, 'left');
    const secondFighterDetails = new FighterDetails(secondFighter, 'right');

    // block handler`s zone :)
    function blockHandler(event) {
      if (playerOnePressBlock(event)) activateFirstFighterBlock();
      if (playerTwoPressBlock(event)) activateSecondFighterBlock();
    }

    function playerOnePressBlock(event) {
      return event.code === controls.PlayerOneBlock;
    }

    function playerTwoPressBlock(event) {
      return event.code === controls.PlayerTwoBlock;
    }

    function activateFirstFighterBlock() {
      firstFighterDetails.blockIsActive = true;
    }

    function activateSecondFighterBlock() {
      secondFighterDetails.blockIsActive = true;
    }

    function unBlockHandler(event) {
      if (playerOnePressBlock(event)) deactivateFirstFighterBlock();
      if (playerTwoPressBlock(event)) deactivateSecondFighterBlock();
    }

    function deactivateFirstFighterBlock() {
      firstFighterDetails.blockIsActive = false;
    }

    function deactivateSecondFighterBlock() {
      secondFighterDetails.blockIsActive = false;
    }

    document.addEventListener('keydown', blockHandler);
    document.addEventListener('keyup', unBlockHandler);

    // simple hit handler`s zone
    function hitHandler(event) {
      if (playerOnePressAttack(event)) applyAttack(firstFighter, secondFighter);
      if (playerTwoPressAttack(event)) applyAttack(secondFighter, firstFighter);
    }

    function playerOnePressAttack(event) {
      return event.code === controls.PlayerOneAttack;
    }

    function playerTwoPressAttack(event) {
      return event.code === controls.PlayerTwoAttack;
    }

    document.addEventListener('keydown', hitHandler);

    function applyAttack(attacker, defender) {
      attack(attacker, defender, false);
      checkWinner();
    }

    function attack(attacker, defender, isCritical) {
      let attackerUseBlock = isFirstFighter(attacker) ? firstFighterDetails.blockIsActive : secondFighterDetails.blockIsActive;
      if (!attackerUseBlock) {
        let damage = isCritical ? getCriticalHitDamage(attacker) : getDamageWithCheckOnBlock(attacker, defender);
        defender.health -= damage;
        updateHealthBar(defender);
      }
    }

    function isFirstFighter(fighter) {
      return fighter._id === firstFighterDetails._id;
    }

    function isSecondFighter(fighter) {
      return fighter._id === secondFighterDetails._id;
    }

    function getDamageWithCheckOnBlock(attacker, defender) {
      if (firstFighterDodgeHit(defender) || secondFighterDodgeHit(defender)) return 0;

      return getDamage(attacker, defender);
    }

    function firstFighterDodgeHit(defender) {
      return isFirstFighter(defender) && firstFighterDetails.blockIsActive;
    }

    function secondFighterDodgeHit(defender) {
      return isSecondFighter(defender) && secondFighterDetails.blockIsActive;
    }

    // critical hit handler`s zone
    const [pOneKey1, pOneKey2, pOneKey3] = controls.PlayerOneCriticalHitCombination;
    const [pTwoKey1, pTwoKey2, pTwoKey3] = controls.PlayerTwoCriticalHitCombination;

    function criticalHitHandler(event) {
      makeCritOnKeys(() => applyCriticalAttack(firstFighter, secondFighter), pOneKey1, pOneKey2, pOneKey3);
      makeCritOnKeys(() => applyCriticalAttack(secondFighter, firstFighter), pTwoKey1, pTwoKey2, pTwoKey3);
    }

    function makeCritOnKeys(func, ...criticalKeysCodes) {
      let pressed = new Set();

      document.addEventListener('keydown', function(event) {
        pressed.add(event.code);

        for (let code of criticalKeysCodes) {
          if (!pressed.has(code)) return;
        }

        pressed.clear();
        func();
      });

      document.addEventListener('keyup', event => pressed.delete(event.code));
    }

    document.addEventListener('keydown', criticalHitHandler);

    function applyCriticalAttack(attacker, defender) {
      attackIfPossible(attacker, defender)
      checkWinner();
    }

    function attackIfPossible(attacker, defender) {
      const currentAttackTime = new Date();
      const canAttack = isFirstFighter(attacker) ? canFirstFighterAttack(currentAttackTime) : canSecondFighterAttack(currentAttackTime);

      if (isFirstFighter(attacker) && canAttack) {
        attack(attacker, defender, true);
        firstFighterDetails.lastCritTime = currentAttackTime;
      }
      if (isSecondFighter(attacker) && canAttack) {
        attack(attacker, defender, true);
        secondFighterDetails.lastCritTime = currentAttackTime;
      }
    }

    function canFirstFighterAttack(currentCritTime) {
      const lastCrit = firstFighterDetails.lastCritTime;
      return lastCrit === undefined || currentCritTime - lastCrit >= 10000;
    }

    function canSecondFighterAttack(currentAttackTime) {
      const lastCrit = secondFighterDetails.lastCritTime;
      return lastCrit === undefined || currentAttackTime - lastCrit >= 10000;
    }

    // health bar control zone
    function updateHealthBar(fighter) {
      let fighterSide = isFirstFighter(fighter) ? firstFighterDetails.side : secondFighterDetails.side;
      let percentOfHealth = getFighterHealthPercent(fighter);
      const healthBarPercent = percentOfHealth >= 0 ? percentOfHealth : 0;

      document.getElementById(`${fighterSide}-fighter-indicator`).style.width = `${healthBarPercent}%`;
    }

    function getFighterHealthPercent(fighter) {
      const maxHealth = isFirstFighter(fighter) ? firstFighterDetails.maxHealth : secondFighterDetails.maxHealth;
      return fighter.health * 100 / maxHealth;
    }

    // check winner & finish game zone
    function checkWinner() {
      if (playerTwoWin()) finishGame(secondFighter);
      if (playerOneWin()) finishGame(firstFighter);
    }

    function playerTwoWin() {
      return firstFighter.health <= 0;
    }

    function playerOneWin() {
      return secondFighter.health <= 0;
    }

    function finishGame(winner) {
      resolve(winner);
    }
  });
}

export function getDamage(attacker, defender) {
  const hitPower = getHitPower(attacker);
  const blockPower = getBlockPower(defender);

  return hitPower > blockPower ? hitPower - blockPower : 0;
}

export function getHitPower(fighter) {
  const criticalHitChance = getRandomNumber(1, 2);
  return fighter.attack * criticalHitChance;
}

function getRandomNumber(min, max) {
  return Math.random() * (max - min) + min;
}

export function getBlockPower(fighter) {
  const dodgeChance = getRandomNumber(1, 2);
  return fighter.defense * dodgeChance;
}

function getCriticalHitDamage(fighter) {
  return 2 * fighter.attack;
}
