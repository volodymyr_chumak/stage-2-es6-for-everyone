import { createElement } from '../helpers/domHelper';

export function createFighterPreview(fighter, position) {
  const positionClassName = position === 'right' ? 'fighter-preview___right' : 'fighter-preview___left';
  const fighterElement = createElement({
    tagName: 'div',
    className: `fighter-preview___root ${positionClassName}`,
  });

  const image = createFighterImage(fighter);
  const name = fighter.name;
  const health = fighter.health;
  const attack = fighter.attack;
  const defense = fighter.defense;

  const infoDiv = createElement({
    tagName: 'div',
    className: 'fighter-preview___characteristics'
  })

  infoDiv.innerHTML = `Name: ${name}<br>Health: ${health}<br>Attack: ${attack}<br>Defense: ${defense}`;

  fighterElement.append(image);
  fighterElement.append(infoDiv);

  return fighterElement;
}

export function createFighterImage(fighter) {
  const { source, name } = fighter;
  const attributes = { 
    src: source, 
    title: name,
    alt: name 
  };
  const imgElement = createElement({
    tagName: 'img',
    className: 'fighter-preview___img',
    attributes,
  });

  return imgElement;
}
