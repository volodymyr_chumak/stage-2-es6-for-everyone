import { showModal } from './modal';

export function showWinnerModal(fighter) {
  let body = document.createElement('div');
  //body.innerHTML = `Health: ${fighter.health} out of ${fighter.maxHealth}<br>Battle duration: ${fighter.battleDuration} seconds`;
  body.innerHTML = `Health: ${fighter.health} out of maxHealth<br>Battle duration: number of seconds`;

  showModal({
    title: `Winner: ${fighter.name}`,
    bodyElement: body
  })
}
